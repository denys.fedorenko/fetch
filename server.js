const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');
const cors = require('cors');
const fs = require('fs');

app.use(cors());

app.use('/form', express.static(path.join(__dirname, '/index.html')));
app.use('/js', express.static(path.join(__dirname, '/src')));
app.use('/files', express.static(path.join(__dirname, '/uploads')));

app.use('/assets', express.static(path.join(__dirname, '/assets')));
app.use('/js/components', express.static(path.join(__dirname, '/components')));
app.use('/src/api', express.static(path.join(__dirname, '/api')));
app.use('/utils', express.static(path.join(__dirname, '/utils')));
app.use('/css', express.static(path.join(__dirname, '/css')));

// default options
app.use(fileUpload());

app.post('/ping', function(req, res) {
  res.send('pong');
});

app.delete('/deleteListItem/:name', function(req, res) {
  const { name } = req.params;

  fs.unlink(path.join(__dirname, `/uploads/${name}`), function(err) {
    if (err) {
      res.sendStatus(500).send(err);
    } else {
      res.send('File was deleted');
    }
  });
});

app.get('/list', function(req, res) {
  fs.readdir(`${__dirname}/uploads`, function(err, items) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(items);
  });
});

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }
  sampleFile = req.files.sampleFile; // eslint-disable-line

  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(path.join('File uploaded to ', uploadPath));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});