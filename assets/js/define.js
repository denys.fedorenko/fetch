function define(...args) {
  const [func, dependencies, name] = args.reverse();
  const dependenciesFromModule = dependencies.map(el => Promise.resolve(define.cache[el]));
  const result = Promise.all(dependenciesFromModule).then(res => func(...res));

  if (name) {
    define.cache[name] = result;
  }
}
define.cache = {};
