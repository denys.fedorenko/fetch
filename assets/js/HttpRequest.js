class HttpRequest {
  // get request options({ baseUrl, headers })
  constructor({ baseUrl, headers }) {
    this.baseUrl = baseUrl;
    this.headers = headers;
  }

  createUrl(url, params) {
    let newUrl = this.baseUrl || '';

    if (url) {
      newUrl += url;
    }

    if (params) {
      newUrl += `${newUrl.include('?') ? '&' : '?'}${new URLSearchParams(params)}`;
    }
    return newUrl;
  }

  get(url, config = {}) {
    return this.request({ ...config, method: 'GET', url });
  }

  post(url, body = {}, config = {}) {
    return this.request({ ...config, method: 'POST', url, body });
  }

  delete(url, config = {}) {
    return this.request({ ...config, method: 'DELETE', url });
  }

  request(config) {
    return new Promise((res, rej) => {
      const {
        data,
        method,
        url,
        body,
        params,
        transformResponse,
        header,
        responseType = 'json',
        onDownloadProgress,
        onUploadProgress
      } = config;


      const xhr = new XMLHttpRequest();
      const sendBody = body || data;
      const requestUrl = this.createUrl(url, params);
      xhr.open(method, requestUrl);
      xhr.responseType = responseType;
      const newHeaders = {
        ...this.headers,
        ...header
      };

      for (const key in newHeaders) {
        xhr.setRequestHeader(key, newHeaders[key]);
      }

      xhr.onload = () => {
        const responseObject = {
          status: xhr.status,
          data: xhr.response,
          type: xhr.responseType
        };

        if (xhr.status >= 400) {
          rej(responseObject);
        }

        if (transformResponse) {
          responseObject.data = transformResponse.reduce((accum, el) => el(accum), responseObject.data);
        }

        res(responseObject);
      };

      if (method === 'POST') {
        xhr.upload.onprogress = onUploadProgress;
        xhr.onerror = rej;
        return xhr.send(sendBody || null);
      }
      xhr.onprogress = onDownloadProgress;
      xhr.onerror = rej;
      xhr.send();
    });
  }
}
window.HttpRequest = HttpRequest;
