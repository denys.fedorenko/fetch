/* eslint-disable no-console */
/* eslint-disable no-alert */
window.define('FileList', [], function () {
  class FileList {
    #fileList = [];
    #listCbOnClick = []
    #parentNode;
    #api;

    constructor(node, api) {
      this.#parentNode = node;
      this.#api = api;
      this.getList();
    }

    getList = () => {
      this.#api.getFileList()
        .then(({ data }) => {
          this.#checkUpdate(data);
        })
        .catch(console.log);
    }

    #checkUpdate = data => {
      // eslint-disable-next-line max-len
      const hasChanged = this.#fileList.length === data.length && this.#fileList.some((el, index) => el === data[index]);

      if (hasChanged) {
        return;
      }
      this.#fileList = [...data];
      this.#render();
    }

    #render = () => {
      this.#parentNode.innerHTML = '';

      const ul = document.createElement('ul');
      ul.classList.add('ul_listFiles');

      for (let i = 0; i < this.#fileList.length; i++) {
        const li = document.createElement('li');
        const p = document.createElement('p');
        const btn = document.createElement('button');

        p.innerText = this.#fileList[i];
        p.classList.add('file__p');

        li.classList.add('file__li');
        li.setAttribute('data-id', i);

        btn.textContent = 'Delete';
        btn.classList.add('btn', 'btn-danger', 'list-buttons');

        p.addEventListener('click', ({ target }) => {
          this.#listCbOnClick.forEach(el => el(target.textContent));
        });

        btn.addEventListener('click', () => {
          this.#removeItemFromList(p.textContent);
        });

        li.append(p, btn);
        ul.appendChild(li);
      }
      this.#parentNode.appendChild(ul);
    }

    #removeItemFromList = li => {
      this.#api.deleteFile(li);
      this.getList();
    }

    onClick = cb => {
      this.#listCbOnClick.push(cb);
    }
  }

  return FileList;
});