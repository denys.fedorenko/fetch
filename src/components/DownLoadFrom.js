/* eslint-disable no-alert */
window.define('DownloadForm', [], function () {
  class DownloadForm {
    #api;
    #parentNode;
    #downloadForm;
    #cbList = [];
    #progress;
    #node;

    constructor(node, api) {
      this.#parentNode = node;
      this.#api = api;
      this.#render();
      this.#downloadForm = this.#parentNode.querySelector('.form-dwn');
      this.#downloadForm.addEventListener('submit', this.#onDownload);
      this.#node = this.#parentNode.querySelector('.get-form');
    }

    #render = () => {
      const form = document.createElement('form');
      form.classList.add('form-dwn');
      form.innerHTML = `<div id='downloadForm' class="input-group mb-3 form">
      <input type="text" name="sampleFile" class="form-control get-form" placeholder="Enter url" aria-label="Enter data"
        aria-describedby="button-addon2">
      <input class="btn btn-primary btn-get form-btn" type="submit" value='Download!' id="button-addon2" />
    </div>`;

      this.#parentNode.appendChild(form);
    }

    #onDownload = e => {
      e.preventDefault();

      if (!this.#node.value) {
        alert('Input is empty, write some data!!!');
        return;
      }

      const configForGet = {
        onDownloadProgress: ({ total, loaded }) => {
          this.#progress({ total, loaded });
        }
      };

      this.#api.downloadFile(this.#node.value, configForGet)
        .then(res => {
          if (!res) {
            return;
          }

          this.#cbList.forEach(el => el(res));
        })
        .catch(({ target }) => {
          if (target >= 400) {
            alert('No such file');
          }
        })
        .finally(this.#resetForm());
    }

    set inputValue(data) {
      this.#node.value = data;
    }

    progress(fn) {
      this.#progress = fn;
    }

    onSubmit = cb => {
      this.#cbList.push(cb);
    }

    #resetForm = () => {
      this.#node.value = '';
    }
  }
  return DownloadForm;
});