window.define('UploadForm', ['ProgressBar'], function(ProgressBar) {
  class UploadForm {
    #api;
    #parentNode;
    #listOfCb = [];
    #progress;

    constructor(node, api) {
      this.#parentNode = node;
      this.#api = api;
      this.#render();
      const progressBar = document.querySelector('.container');
      this.#progress = new ProgressBar(progressBar, {
        defaultTimetoDelete: 3000,
        timeToDelete: 3000,
        color: 'red',
        top: '7%'
      });

      const uploadForm = document.querySelector('.upload-form');
      this.form = this.#parentNode.querySelector('.form-control');
      uploadForm.addEventListener('submit', e => {
        this.#uploadFile(e, 'sampleFile');
      });
    }

    #render = () => {
      const form = document.createElement('form');
      form.classList.add('upload-form');
      form.innerHTML = `<div id='uploadForm' encType="multipart/form-data" class="input-group mb-3 uploadForm form">
      <input type="file" name="sampleFile" class="form-control" placeholder="Enter data" aria-label="Enter data"
        aria-describedby="button-addon2" />
      <input type='submit' value='Upload!' class="btn btn-primary form-btn" id="button-addon2" />
    </div>
    <div class="container"></div>`;
      this.#parentNode.appendChild(form);
    }

    #uploadFile = (e, name) => {
      e.preventDefault();
      const file = new FormData();
      const uploadFile = e.target.sampleFile;
      file.append(name, uploadFile.files[0]);

      if (!uploadFile.files[0]) {
        // eslint-disable-next-line no-alert
        alert('Empty field');
        return;
      }

      this.#api.uploadFile(file, {
        onUploadProgress: ({ total, loaded }) => {
          this.#progress.setPercent = { total, loaded };
        }
      }).then(({ data }) => {
        this.#listOfCb.forEach(el => el(data));
      });
      this.clearInputValue();
      this.#progress.deleteProgressBar();
    }

    clearInputValue = () => {
      this.form.value = '';
    }

    onSubmit = cb => {
      this.#listOfCb.push(cb);
    }
  }

  return UploadForm;
});