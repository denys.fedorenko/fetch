window.define('ProgressBar', [], function() {
  class ProgressBar {
    #timeToDelete;
    #defaultTimetoDelete;
    #parentNode;
    #percent;
    #color;
    #top;

    constructor(node, config) {
      this.#parentNode = node;
      this.#defaultTimetoDelete = config.defaultTimetoDelete || 3000;
      this.#timeToDelete = config.timeToDelete || this.#defaultTimetoDelete;
      this.#color = config.color || 'blue';
      this.#top = config.top || '27%';
      this.#render();
    }

    set setPercent({ loaded, total }) {
      this.#percent = ((loaded / total) * 100).toFixed(0);
      this.#updateProgressPercent();

      if (this.#percent === '100') {
        this.deleteProgressBar();
      }
    }

    #updateProgressPercent = () => {
      this.#render();

      this.#parentNode.style.top = this.#top;
      this.#parentNode.style.display = 'block';
      const currentPercent = this.#parentNode.querySelector('.progress-bar');
      currentPercent.style.width = `${this.#percent}%`;
      currentPercent.setAttribute('aria-valuenow', this.#percent);
      currentPercent.textContent = `${this.#percent}`;
      currentPercent.style.background = `${this.#color}`;
    }

    #render = () => {
      this.#parentNode.innerHTML = ` 
      <div class="progress">
      <div class="progress-bar upload-progress" role="progressbar" aria-valuenow="70" 
      aria-valuemin="0" aria-valuemax="100">
      0%
      </div>
      </div>`;
    }

    deleteProgressBar() {
      setTimeout(() => {
        this.#parentNode.innerHTML = '';
      }, this.#timeToDelete);
    }
  }

  return ProgressBar;
});