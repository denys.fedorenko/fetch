window.define('Picture', [], function() {
  class Picture {
    #src;
    #parentNode;
    #nodeImg;

    constructor(node) {
      this.#parentNode = node;
      this.#render();
    }

    set src(data) {
      this.#src = URL.createObjectURL(data);
      this.#update();
    }

    #render = () => {
      this.#nodeImg = document.createElement('img');
      this.#nodeImg.classList.add('donwloadImg');
      this.#parentNode.append(this.#nodeImg);
    }

    #update = () => {
      this.#nodeImg.src = this.#src;
    }
  }

  return Picture;
});

