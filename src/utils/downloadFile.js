window.define('downLoadFile', [], function() {
  function dwnLoadFile(res) {
    const downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);
    downloadLink.style = 'display: none';
    downloadLink.href = res.url;
    downloadLink.download = '';
    downloadLink.click();
    downloadLink.remove();
  }
  return dwnLoadFile;
});
