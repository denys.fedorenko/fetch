window.define('ApiService', ['HttpRequest'], function(HttpRequest) {
  class ApiService {
    constructor() {
      this.hxr = new window.HttpRequest({ baseUrl: 'http://localhost:8000' });
    }

    uploadFile(file, config) {
      return this.hxr.post('/upload', file, config);
    }

    downloadFile(fileName, config) {
      return this.hxr.get(`/files/${fileName}`, {
        responseType: 'blob',
        ...config
      });
    }

    getFileList() {
      return this.hxr.get('/list');
    }

    deleteFile(fileName, config) {
      return this.hxr.delete(`/deleteListItem/${fileName}`, config);
    }
  }

  return ApiService;
});
