window.define(['Picture', 'FileList', 'UploadForm', 'DownloadForm', 'ProgressBar', 'ApiService', 'downLoadFile'],
  function(...args) {
    const [Picture, FileList, UploadForm, DownloadForm, ProgressBar, ApiService, dwnLoadFile] = args;

    const app = document.querySelector('.app');
    const imgWrap = document.createElement('div');
    const progressBar = document.createElement('div');
    const fileList = document.createElement('div');
    const forms = document.createElement('div');

    imgWrap.classList.add('donwloadImgWrapper');
    fileList.classList.add('list-block');
    forms.classList.add('forms-wrapper');
    progressBar.classList.add('container');

    app.append(progressBar, fileList, forms);


    const api = new ApiService();
    const picture = new Picture(imgWrap);
    const newProgress = new ProgressBar(progressBar, { timeToDelete: 5000 });
    const list = new FileList(fileList, api);
    const uploadForm = new UploadForm(forms, api);
    const dwnloadForm = new DownloadForm(forms, api);
    forms.append(imgWrap);

    dwnloadForm.onSubmit(({ data }) => {
      if (!data.type.includes('image')) {
        return dwnLoadFile(data);
      }

      picture.src = data;
    });

    dwnloadForm.onSubmit(newProgress.deleteProgressBar);
    dwnloadForm.progress(data => {
      newProgress.setPercent = data;
    });

    uploadForm.onSubmit(list.getList);

    list.onClick(data => {
      dwnloadForm.inputValue = data;
    });
  });